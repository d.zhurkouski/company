from django.urls import path, include
from rest_framework import routers

from .views import CompanyViewSet, ManagerViewSet, ActivityViewSet, SocialNetworkViewSet

router = routers.SimpleRouter()
router.register(r'organizations', CompanyViewSet)
router.register(r'managers', ManagerViewSet)
router.register(r'activities', ActivityViewSet)
router.register(r'socialnetworks', SocialNetworkViewSet)
urlpatterns = router.urls

urlpatterns = [
    path('', include(router.urls)),
]
