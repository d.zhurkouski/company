from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=100)
    created_date = models.DateField()
    official_site = models.URLField(max_length=100)

    def __str__(self):
        return self.name


class Manager(models.Model):
    name = models.CharField(max_length=100)
    company_id = models.ForeignKey(Company, on_delete=models.CASCADE)


class Activity(models.Model):
    name = models.CharField(max_length=100, unique=True)
    companies = models.ManyToManyField(Company, related_name='activities')


class SocialNetwork(models.Model):
    href = models.URLField(max_length=100)
    company_id = models.ForeignKey(Company, on_delete=models.CASCADE)
