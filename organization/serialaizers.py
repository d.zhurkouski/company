from rest_framework import serializers

from .models import Company, Manager, Activity, SocialNetwork


class CompanySerializer(serializers.ModelSerializer):
    # activities = serializers.PrimaryKeyRelatedField(queryset=Activity.objects.all(), many=True)

    class Meta:
        model = Company
        fields = (
            'name',
            'created_date',
            'official_site',
            'activities'
        )


class ManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manager
        fields = (
            'name',
            'company_id'
        )


class ActivitySerializer(serializers.ModelSerializer):
    companies = serializers.PrimaryKeyRelatedField(queryset=Company.objects.all(), many=True)

    class Meta:
        model = Activity
        fields = (
            'name',
            'companies',
        )


class SocialNetworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialNetwork
        fields = (
            'href',
            'company_id'
        )
